db.users.insertOne({
			name: "Single",
			accomodation: 2,
			price: 1000,
			description: "A simple room with all the basic necessities",
			roomsAvailable: 10,
			isAvailable: false
		});

db.users.insertMany([{
	name: "Double",
	accomodation: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	roomsAvailable: 5,
	isAvailable: false
},
{
	name: "Queen",
	accomodation: 4,
	price: 4000,
	description: "A room with queen sized bed perfect for a simple gateway",
	roomsAvailable: 15,
	isAvailable: false
}
]);

db.users.find({ name: "Double" });

db.users.updateOne(
	{ name: "Queen" },
	{
		$set: {
			roomsAvailable: 0,
		}
	}
);

db.users.deleteMany({
	roomsAvailable: 0
});
